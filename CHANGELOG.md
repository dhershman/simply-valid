# Change Log v2+

## v2.0.0 - Quality of Life update

> - MAJOR re write of entire module
> - Method chaining is no longer supported to keep chains stay on v1.3.1!
> - Reduced validation methods to simple logic for large performance boosts
> - Dynamically writes validation story so as they fail they're recorded
> - Simplified Usage
> - Changed the `toMatch` property to `equalTo` in the options
> - Organized Methods based on their types `has`, `is`, etc...
> - This allowed large amounts of code cleanup in the main index file
> - Changed `matchesGiven` to `isEqual`
> - Removed `matchesCustom`
> - Added method `noLetters`
